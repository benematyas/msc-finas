import cv2
from cv2 import aruco


def createCharucoBoard():
    aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)
    board = aruco.CharucoBoard_create(11, 8, 1, 0.76, aruco_dict)
    img = board.draw((2000, 1500))
    cv2.imwrite("CharucoBoard.png", img)

if __name__=="__main__":
    createCharucoBoard()