import numpy as np
import cv2
from cv2 import aruco
from scipy.spatial import distance as dist

aruco_dict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_4X4_50)

def load_image(fiename, saveLocation):
    frame = cv2.imread(fiename)

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    frame=cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

    corners, ids = detect_markers(gray)

    markerCorners = get_markers_corners(corners)
    #warp image
    flattenIds = ids.flatten()
    refPoints = []
    for aruco_id in (1, 2, 3, 4):
        j = np.squeeze(np.where(flattenIds == aruco_id))
        corner = np.squeeze(markerCorners[j])
        refPoints.append(corner)

    (refPtTL, refPtTR, refPtBL, refPtBR) = refPoints
    dstMat = [refPtTL, refPtTR, refPtBL, refPtBR]
    dstMat =  np.float32(dstMat)

    trgW = 850 #1mm=10px
    trgH  = 1030 #1mm=10px

    srcMat =  np.float32([[0, 0], [trgW, 0], [0, trgH], [trgW, trgH]])

    M = cv2.getPerspectiveTransform(dstMat, srcMat)
    warped_img = cv2.warpPerspective(frame, M, (trgW, trgH))

    cv2.imwrite(saveLocation, cv2.cvtColor(warped_img, cv2.COLOR_RGB2BGR)) 
    

def detect_markers(frame,camera_matrix, dist_coeff):
    parameters = aruco.DetectorParameters_create()

    if camera_matrix== None:
            corners, ids, _ = aruco.detectMarkers(
            frame, aruco_dict, parameters=parameters)
    else:
         corners, ids, _ = aruco.detectMarkers(
            frame, aruco_dict, parameters=parameters,cameraMatrix=camera_matrix,distCoeff=dist_coeff)

    return corners, ids


def get_markers_corners(markers):
    markers2 = np.array([c[0] for c in markers])
    origos = markers2.mean(axis=1)

    markerNeededCorners = []
    for index in range(len(markers)):
        marker = markers2[index]
        markerPointDistances = []
        for markerPoint in marker:
            distances = []
            for origoIndex in range(len(origos)):
                if origoIndex != index:
                    distance = calc_euclidean_distance(markerPoint, origos[origoIndex])
                    distances.append(distance)
            markerPointDistances.append((np.array(distances).mean()))
        markerNeededCorners.append(marker[np.array(markerPointDistances).argmax()])

    return markerNeededCorners

def calc_euclidean_distance(aPoint, bPoint):
    return dist.euclidean((aPoint[0], aPoint[1]), (bPoint[0], bPoint[1]))

if __name__ == '__main__':
    load_image("correct.png",'warped_img.png')