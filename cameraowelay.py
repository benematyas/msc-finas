import numpy as np
import cv2
from cv2 import aruco
from scipy.spatial import distance as dist


aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)
maxdistance = 15
# font
font = cv2.FONT_HERSHEY_SIMPLEX
# org
org = (50, 50)
# fontScale
fontScale = 1
# Blue color in BGR
color = (255, 0, 0)
# Line thickness of 2 px
thickness = 2


def calculateDistance(fromPoint, toPoint):
    return dist.euclidean(fromPoint, toPoint)


def detect_markers(img, camera_matrix, dist_coeff):
    aruco_list = []
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    parameters = aruco.DetectorParameters_create()
    if camera_matrix== None:
        corners, ids, _ = aruco.detectMarkers(
            gray, aruco_dict, parameters=parameters)
    else:
         corners, ids, _ = aruco.detectMarkers(
            gray, aruco_dict, parameters=parameters,cameraMatrix=camera_matrix,distCoeff=dist_coeff)

    if ids is None:
        return []
    centrecoord = []
    L = len(ids)

    for i in range(0, L):
        x = int(corners[i][0][0][0]+corners[i][0][1]
                [0]+corners[i][0][3][0]+corners[i][0][2][0])/4
        y = int(corners[i][0][0][1]+corners[i][0][1]
                [1]+corners[i][0][3][1]+corners[i][0][2][1])/4
        p = (x, y)
        centrecoord.append(p)
    for i in range(0, L):
        p = (ids[i][0], centrecoord[i])

        aruco_list.append(p)
    return aruco_list


def draw_marker(frame, markerList):
    for marker in markerList:

        frame = cv2.circle(frame, (int(marker[1][0]), int(
            marker[1][1])), 10, (0, 0, 255), -1)
    return frame


def draw_Line(frame, origMarker, currentmarker):

    return cv2.line(frame, (int(origMarker[1][0]), int(origMarker[1][1])), (int(currentmarker[1][0]), int(currentmarker[1][1])), (255, 0, 0), 5)


def chekFrame(frame,originalFrame, originalList, currentList):

    L = len(currentList)
    correct = True
    for i in range(0, L):
        currentmarker = currentList[i]

        origMarker = None
        for originalM in originalList:
            if originalM[0] == currentmarker[0]:
                origMarker = originalM

        distance = calculateDistance(origMarker[1], currentmarker[1])
        frame = draw_Line(frame, origMarker, currentmarker)
        if(distance > maxdistance):
            correct = False

    if L < 4:
        frame = cv2.putText(frame, "not all marker are visible", org, font,
                            fontScale, color, thickness, cv2.LINE_AA)
        return frame
    if correct:
        frame = cv2.putText(frame, "Correct", org, font,
                            fontScale, (0, 255, 0), thickness, cv2.LINE_AA)
        cv2.imwrite("correct.png", originalFrame)
        cv2.imwrite("correct_with overlay.png", frame)
    else:
        frame = cv2.putText(frame, "Try closer", org, font,
                            fontScale, (0, 0, 255), thickness, cv2.LINE_AA)

    return frame


def main(camera_matrix, dist_coeff):
    overlay = cv2.imread('data\images\cropped.png', cv2.IMREAD_UNCHANGED)
    rows, cols, channels = overlay.shape
    aspectratio = cols/rows
    print(overlay.shape)
    cap = cv2.VideoCapture(0)
    ret, frame = cap.read()
    rowsframe, colsframe, channelsframe = frame.shape
    portrait = colsframe < rowsframe
    overlayNewSize = (0, 0)
    if not portrait:
        overlay = cv2.rotate(overlay, cv2.ROTATE_90_CLOCKWISE)
        width = int(colsframe*0.8)
        height = int(width*aspectratio)
        overlayNewSize = (width, height)

    else:
        width = int(colsframe*0.8)
        height = int(width*aspectratio)
        overlayNewSize = (width, height)

    print(overlayNewSize)
    print(overlay.shape)
    overlay = cv2.resize(overlay, dsize=overlayNewSize,
                         interpolation=cv2.INTER_AREA)
    print(overlay.shape)
    rows, cols, channels = overlay.shape

    xx = (colsframe - cols) // 2
    yy = (rowsframe - rows) // 2

    top = xx
    bottom = xx+(colsframe-((xx*2)+cols))
    left = yy
    right = yy+(rowsframe-((yy*2)+rows))
    overlay = cv2.copyMakeBorder(
        overlay,  left, right, top, bottom, borderType=cv2.BORDER_CONSTANT, value=[0, 0, 0, 255])
    alpha = overlay[:, :, 3]
    overlay = cv2.bitwise_or(overlay, overlay, mask=alpha)
    overlay = cv2.cvtColor(overlay, cv2.COLOR_RGBA2RGB)

    originalMarkerList = detect_markers(overlay, None, None)

    while(True):

        # copy img image into center of result image

        ret, originalFrame = cap.read()

        markerList = detect_markers(originalFrame, camera_matrix, dist_coeff)

        frame = cv2.addWeighted(originalFrame, 1, overlay, 0.5, 0)

        frame = draw_marker(frame, markerList)

        chekFrame(frame,originalFrame, originalMarkerList, markerList)

        # Display the resulting frame
        cv2.imshow('frame', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    main(None,None)
